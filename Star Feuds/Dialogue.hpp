//
//  Dialogue.hpp
//  Dialogue
//
//  Created by William Molina on 2/22/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#ifndef Dialogue_hpp
#define Dialogue_hpp

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <Kairos/Timestep.hpp>
#include <Kairos/FpsLite.hpp>
#include <fstream>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>
#include "ResourcePath.hpp"

using namespace std;

class Dialogue {
private:
    
    sf::Vector2f mWindowSize;
    
    sf::Vector2f linearInterpolation(sf::Vector2f start, sf::Vector2f end, float alpha)
    {
        return (start * (1 - alpha) + end * alpha);
    }
    
    enum state{INTRO = 1, FINDTEXT, GETTEXT, EXIT};
    int step = 1; // 1 for auto-start dialogue, 0 for press-space-to-start
    
    sf::Texture characterTexture;
    sf::Sprite player, enemy;
    sf::Vector2f playerPreviousPosition, playerCurrentPosition;
    sf::Vector2f enemyPreviousPosition, enemyCurrentPosition;
    sf::Vector2f dialogueBoxPreviousPosition, dialogueBoxCurrentPosition, dialogueBoxFinalPosition;
    float actorMoveSpeed = 200.f;
    float moveRate = 1.75;
    
    sf::SoundBuffer speech1Buffer, speech2Buffer;
    sf::Sound speech;
    
    sf::Font font;
    sf::RectangleShape dialogueBox, actorLabelBox;
    sf::Vector2f boxPadding;
    fstream ifs{resourcePath() + "Text", fstream::in};
    
    // ****************************** find the string
    unsigned int curLine = 0;
    string line = "";
    string actorName = "";
    
    // ****************************** text variables
    sf::Text text1, text2, text3, actorLabel, arrow;
    char ch = '\0';
    
    string firstLine, secondLine, thirdLine;
    float scrollAmount = 35.f;
    sf::Time accumulator = sf::Time::Zero;
    sf::Time prevAccumulator;
    
    sf::Time arrowAccumulator = sf::Time::Zero;
    sf::Time prevArrowAccumulator;
    
    sf::Time frameTime = sf::seconds(.016f);
    sf::Time arrowFrameTime = sf::seconds(.3f);
    sf::Time arrowMaxTime = sf::seconds(.6f);
    
    bool buttonPressed = false;
    bool getOneP = true;
    bool displayFirstLine = true;
    bool foundStartText = false;
    bool isDone = false;
    
    bool arrowIsAnimated = false;
    bool displayArrow = false;
    
    int lineCount = 0;
    int charCount = 0;
    
public:
    Dialogue(sf::Vector2u &window);
    void initiate();
    void playIntro(kairos::Timestep &timestep);
    void exit(kairos::Timestep &timestep);
    void animateArrow(double &clock);
    void setActorLabelBox(string actor);
    bool textWasFound();
    void findStartText(string search);
    void startDialogue();
    void streamText();
    int getState();
    bool getIsDone();
    void getNewParagraph(double &clock);
    void update(string search, kairos::Timestep &timestep, double &clock);
    void draw(sf::RenderWindow &window);
    
};

#endif /* Dialogue_hpp */
