//
//  Bullet.cpp
//  Star Feuds
//
//  Created by William Molina on 2/15/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#include "Bullet.hpp"
Bullet::Bullet(sf::Vector2f &shooterPosition, float outsideSpeed) {

    speed = outsideSpeed;
    currentPosition.x = shooterPosition.x + 23;
    currentPosition.y = shooterPosition.y + 5;
    bullet.setSize(sf::Vector2f(8, 24));
    bullet.setFillColor(sf::Color::Red);
    bullet.setPosition(currentPosition);
}

//-----------------------------------------------------------
sf::Vector2f Bullet::getPosition() {
    return currentPosition;
}

//-----------------------------------------------------------
sf::FloatRect Bullet::getBulletBox() {
    
    bulletBox.left = bullet.getPosition().x;
    bulletBox.top = bullet.getPosition().y;
    bulletBox.width = bullet.getGlobalBounds().width;
    bulletBox.height = bullet.getGlobalBounds().height;
    
    return bulletBox;
}

//-----------------------------------------------------------
void Bullet::update(int level, int direction, kairos::Timestep &timestep, double &clock) {
    
    previousPosition = currentPosition;
    
    float dt{ timestep.getStepAsFloat() };
    
    currentPosition.y -= speed * direction * dt;
    
    float interpolationAlpha{ timestep.getInterpolationAlphaAsFloat() };
    
    bullet.setPosition(linearInterpolation(previousPosition, currentPosition, interpolationAlpha += 1));
    
    if (level != 5) {
        if (!soundPlayed) {
            laserBuffer.loadFromFile(resourcePath() + "laser.wav");
            laser.setBuffer(laserBuffer);
            laser.setVolume(55);
            laser.play();
            soundPlayed = true;
        }
    }
    
    bulletBox.left = currentPosition.x;
    bulletBox.top = currentPosition.y;
}

//-----------------------------------------------------------
void Bullet::draw(sf::RenderWindow &window) {
    window.draw(bullet);
    
}