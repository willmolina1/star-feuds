//
//  Explosion.cpp
//  Star Feuds
//
//  Created by William Molina on 2/15/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#include "Explosion.hpp"

using namespace std;

Explosion::Explosion(int act, sf::Vector2f position, sf::Vector2f size, int randNum) {
    actor = act;
    sprite.setOrigin(sprite.getLocalBounds().width / 2, sprite.getLocalBounds().height / 2);
    sprite.setRotation(randNum);
    sprite.setPosition(position.x + (size.x / 2), position.y + (size.x / 2));
}

//-----------------------------------------------------------
//void Explosion::explode(sf::Vector2f &position, sf::Vector2f size, sf::View &view) {
void Explosion::update(double &clock, sf::View &view) {
    
    if (!initialized) {
        if (!texture.loadFromFile(resourcePath() + "explosion.png")) {
            cout << "Error loading explosion texture!" << endl;
        }
        sprite.setTexture(texture);
        sprite.setTextureRect(sf::IntRect(0, 0, 48, 48));
        initialized = true;
    }
    
    if (playExplosion) {
//        accumulator += clock.restart();
        accumulator += sf::seconds(clock) - prevTime;
        prevTime = sf::seconds(clock);
        
        sprite.setTextureRect(sf::IntRect(48 * frame, 0, 48, 48));
        
        if (accumulator > frameTime) {
            frame++;
            if (frame == 8) {
                playExplosion = false;
            }
            accumulator = sf::Time::Zero;
        }
        
        if (!playedSound) {
            if (!explosionBuffer.loadFromFile(resourcePath() + "explosion" + to_string(actor) + ".wav")) {
                cout << "Error loading explosion file!" << endl;
            }
            explosionSound.setBuffer(explosionBuffer);
            if (actor == 0) {
                explosionSound.setVolume(20);
            } else if (actor == 1) {
                explosionSound.setVolume(40);
            } else {
                explosionSound.setVolume(50);
            }
            explosionSound.play();
            playedSound = true;
        }
    
// SCREENSHAKE!!!!!!!!!!!!!!!!
        if (actor > 0) {
            if (frame % 2 == 0) {
                view.setCenter(402, view.getCenter().y);
            }
            
            if (frame % 2 == 1) {
                view.setCenter(398, view.getCenter().y);
            }
        }
    }
}

//-----------------------------------------------------------
bool Explosion::isOver() {
    // Return isOver() when the sound finishes playing,
    // which is longer than frame == 8
//    return (explosionSound.getStatus() == 0);
    
    if (actor == 0) {
        return (frame == 8);
//                return (explosionSound.getStatus() == 0);
    } else {
        return (explosionSound.getStatus() == 0);
    }
    
}
//-----------------------------------------------------------
void Explosion::draw(sf::RenderWindow &window) {
    if (playExplosion) {
        window.draw(sprite);
    }
}