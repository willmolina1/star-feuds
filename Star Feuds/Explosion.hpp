//
//  Explosion.hpp
//  Star Feuds
//
//  Created by William Molina on 2/15/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#ifndef Explosion_hpp
#define Explosion_hpp
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <Kairos/Timestep.hpp>
#include <Kairos/FpsLite.hpp>
#include <cmath>
#include <deque>
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include "ResourcePath.hpp"

using namespace std;

class Explosion {
protected:
    sf::Texture texture;
    sf::Sprite sprite;
    sf::Time accumulator = sf::Time::Zero;
    sf::Time prevTime;
    sf::Time frameTime = sf::seconds(.07f);
    
    int actor;
    
    int frame = -1;
    bool initialized = false;
    bool playExplosion = true;
    bool playedSound = false;
    sf::SoundBuffer explosionBuffer;
    sf::Sound explosionSound;
    sf::Vector2f screenShake;
    
public:
    
    Explosion(int act, sf::Vector2f position, sf::Vector2f size, int randNum);
    void update(double &clock, sf::View &view);
    bool isOver();
    void draw(sf::RenderWindow &window);
    
};


#endif /* Explosion_hpp */
