//
//  Boss.hpp
//  Star Feuds
//
//  Created by William Molina on 6/2/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#ifndef Boss_hpp
#define Boss_hpp

#include "Enemy.hpp"

class Boss : public Enemy {
    
private:
    
    sf::Time timerChangeDirection = sf::seconds(1);
    sf::Time prevChangeDirection;
    
    
public:
    Boss(sf::Vector2f position, int level, float speed) : Enemy(position, level, speed)
    {   srand(static_cast<unsigned int>(time(NULL)));
        
        health = 300;
        
        moveSpeed = 290;
        
        shootChance = 50;
        
        spriteSize = sf::Vector2f(55.f, 55.f);
        
        sprite.setPosition(position);
        
        currentPosition = sprite.getPosition();
        
        if (!texture.loadFromFile(resourcePath() + "enemies.png")) {
            cout << "Error loading ship texture!" << endl;
        }
        
        sound = 2;
    };
    
    void die(sf::View &view);
    void direction(double &clock);
    void setDirection(int dir);
    
};

#endif /* Boss_hpp */
