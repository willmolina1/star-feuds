//
//  Game.hpp
//  Star Feuds
//
//  Created by William Molina on 5/26/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include <algorithm>
#include "Credits.h"
#include "Dialogue.hpp"
#include "Gameplay.hpp"


class Game {
    
private:
    
    sf::RenderWindow window;
    sf::Vector2u winSize{sf::Vector2u(800,600)};
    sf::Image icon;
    sf::View view;
    
    //Kairo stuff BEGIN --------------------------
    const std::string windowTitle{ "Star Feuds" };
    kairos::Timestep timestep;
    kairos::FpsLite fps;
    //Kairo stuff END ----------------------------
    
    sf::Font fontTitle;
    sf::Font font;
    
    sf::Text textTitle, textPressStart, text1, text2, text3;
    
    string line, stringScores;
    int numScores;
    
    int lives;
    int score;
    int level;
    int ngPlus;
    
//    sf::Clock clock; // instead, use the timestep
    sf::Time timerIntro = sf::seconds(2);
    sf::Time prevTimerIntro;
    bool initPrevTimerIntro = false;
    
//    sf::Event::EventType curEvent;
//    sf::Event::EventType prevEvent = ;
    
    bool selectionPressed = false;
    bool paused = false;
    bool pausePressed = false;
    int gameState = 0; // temp
    int prevGameState = -1; // initialize with -1
    int drawState = gameState;
    enum states{TITLE, INTRO, DIALOGUE, GAMEPLAY, GAMEOVER, HIGHSCORE, CREDITS};
    
    vector< Gameplay > gameplay;
    vector< Dialogue > dialogue;
    deque< Credits > credits;
    vector< int > highScores;
    
    void loadCurrentScores();
    void updateHighScores();
    void swapHighScores();
    void saveNewHighScores();
    void displayHighScores();
    
public:
    Game();
    void processEvents();
    void initialize();
    void goToTitleScreen();
    void goToIntro();
    void goToDialogue();
    void goToGamePlay();
    void goToGameOver();
    void goToHighScore();
    void goToCredits();
    void run();
    void draw();
    
};

#endif /* Game_hpp */
