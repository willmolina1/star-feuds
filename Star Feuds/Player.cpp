//
//  Player.cpp
//  Star Feuds
//
//  Created by William Molina on 2/15/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#include "Player.hpp"

Player::Player() {
    if (!texture.loadFromFile(resourcePath() + "ship.png")) {
        cout << "Error loading ship texture!" << endl;
    }
    if (!afterBurnerTexture.loadFromFile(resourcePath() + "flames.png")) {
        cout << "Error loading afterburner texture!" << endl;
    }
}

//-----------------------------------------------------------
void Player::init(int explosions, sf::Vector2u window) {
    
    windowSize = window;
    spriteSize = sf::Vector2f(31.5f, 44.f);
    numExplosions = explosions;
    spriteDirection = 1;
    
    // Ship stuff
    sprite.setTexture(texture);
    sprite.setTextureRect(sf::IntRect(0, 0, spriteSize.x, spriteSize.y));
    sprite.setPosition(((windowSize.x / 2) - (spriteSize.x / 2)), windowSize.y - (spriteSize.y + bottomBuffer));
    currentPosition = sprite.getPosition();

    collider.setOrigin(collider.getLocalBounds().width / 2, collider.getLocalBounds().height / 2);
    collider.setPosition(sf::Vector2f(sprite.getPosition().x + 5.f, sprite.getPosition().y));
    collider.setSize(sf::Vector2f(sprite.getLocalBounds().width / 2, sprite.getLocalBounds().height / 3));
    collider.setOutlineColor(sf::Color::Red);
    collider.setOutlineThickness(1.f);
    collider.setFillColor(sf::Color::Transparent);
    
    // Afterburner stuff
    afterBurnerSprite.setTexture(afterBurnerTexture);
    afterBurnerSprite.setTextureRect(sf::IntRect(0, 0, 34, 64));
    afterBurnerSprite.setOrigin(afterBurnerSprite.getLocalBounds().width / 2,
                                afterBurnerSprite.getLocalBounds().height / 2);
    afterBurnerSprite.setPosition((currentPosition.x + (spriteSize.x / 2)),
                                  currentPosition.y + (spriteSize.y + 20));
    burnerCurrent = afterBurnerSprite.getPosition();
    
    hitBuffer.loadFromFile(resourcePath() + "hit.wav");
    hit.setBuffer(hitBuffer);
}

//-----------------------------------------------------------
bool Player::isAlive() {
    if (health > 0) {
        return true;
    } else {
        return false;
    }
}

//-----------------------------------------------------------
const sf::Vector2f Player::getPosition() {
    sf::Vector2f temp;
    temp.x = sprite.getGlobalBounds().left - 10;
    temp.y = sprite.getGlobalBounds().top;
    return temp;
}

//-----------------------------------------------------------
sf::Vector2f Player::getSize() {
    return sf::Vector2f(sprite.getGlobalBounds().width, sprite.getGlobalBounds().height);
}

//-----------------------------------------------------------
sf::Sprite Player::getSprite() {
    return sprite;
}

//-----------------------------------------------------------
int Player::rotate(double clock) {
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        frameTime +=  sf::seconds(clock);
        spriteDirection = 1;
        if (frameTime > frameSwitch) {
            return 3;
        }
        moveAbit = -3.f;
        return 2;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        frameTime += sf::seconds(clock);
        spriteDirection = -1;
        if (frameTime > frameSwitch) {
            return 4;
        }
        moveAbit = 3.f;
        return 3;
    }
    
    frameTime = sf::Time::Zero;
    spriteDirection = 1;
    moveAbit = 0;
    
    return 0;
}

//-----------------------------------------------------------
int Player::getHealth() {
    return health;
}

//-----------------------------------------------------------
void Player::takeDamage(int damage) {
    health -= damage;
    hit.play();
}

//-----------------------------------------------------------
bool Player::collisionWithBullet(sf::FloatRect &bullet) {
    
//    if ((bullet.top) > sprite.getPosition().y) {

        if ((bullet.top) > collider.getPosition().y) {
//        if (sprite.getPosition().x < bullet.left + bullet.width &&
//            sprite.getPosition().x + sprite.getLocalBounds().width > bullet.left) {
        
        if (collider.getPosition().x < bullet.left + bullet.width &&
            collider.getPosition().x + collider.getLocalBounds().width > bullet.left) {

            health -= 10;
            
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
    
}

//-----------------------------------------------------------
bool Player::explosionIsOver() {
    return explosionOver;
}

//-----------------------------------------------------------
void Player::die(double clock, sf::View &view) {
    
    if (!initBlast) {
//        accumulator += clock.getElapsedTime();
        accumulator += sf::seconds(clock) - prevTime;
        prevTime = sf::seconds(clock);
        
        if (accumulator > explosionTimer) {
            if (numExplosions > 0) {
                
                int rotation = rand() % 360;
                float posX = currentPosition.x + (rand() % 2);
                float posY = currentPosition.y + (rand() % 2);
                blast.push_back(Explosion(1, sf::Vector2f(posX, posY), spriteSize, rotation));

                numExplosions--;
                accumulator = sf::Time::Zero;
                
            } else {
                initBlast = true;
            }
        }
    }
    
    if (!blast.empty()) {
        for (unsigned int i = 0; i < blast.size(); i++) {
            blast.at(i).update(clock, view);
            
            if (blast.at(i).isOver()) {
                blast.pop_front();
            }
        }
    } else {
            explosionOver = true;
    }
}

//-----------------------------------------------------------
void Player::move(kairos::Timestep &timestep, double clock) {
    
    previousPosition = currentPosition;
    burnerPrev = burnerCurrent;
    
    float dt{ timestep.getStepAsFloat() };
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && (sprite.getPosition().x > 0)) {
        currentPosition.x -= moveSpeed * dt;
        burnerCurrent.x -= moveSpeed * dt;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && ((sprite.getPosition().x + spriteSize.x) < windowSize.x)) {
        currentPosition.x += moveSpeed * dt;
        burnerCurrent.x += moveSpeed * dt;
    }
    
//    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
//        currentPosition.y -= moveSpeed * dt;
//    }
//    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
//        currentPosition.y += moveSpeed * dt;
//    }
    
//    sprite.setTextureRect(sf::IntRect(rotate(clock) * spriteSize.x, 0, spriteSize.x * spriteDirection, spriteSize.y));
    
    float interpolationAlpha{ timestep.getInterpolationAlphaAsFloat() };
    
    sprite.setPosition(linearInterpolation(previousPosition, currentPosition, interpolationAlpha));
    afterBurnerSprite.setPosition(linearInterpolation(burnerPrev, burnerCurrent, interpolationAlpha));
    collider.setPosition(sf::Vector2f(sprite.getPosition().x + 8, sprite.getPosition().y + 25));
    animateAfterBurner(clock);
}

//-----------------------------------------------------------
void Player::animateAfterBurner(double clock) {
    
//    afterAccumulator += sf::seconds(clock);
    afterAccumulator += sf::seconds(clock) - afterPrevTimer;
    afterPrevTimer = sf::seconds(clock);
    
    afterBurnerSprite.setTextureRect(sf::IntRect(34 * afterFrame, 0, 34, 64));
    
    if (afterAccumulator > afterFrameTime) {
        afterFrame++;
        if (afterFrame == 8) {
            afterFrame = 0;
        }
        afterAccumulator = sf::Time::Zero;
    }
}

//-----------------------------------------------------------
void Player::update(kairos::Timestep &timestep, sf::View &view, double clock) {
    
    if (isAlive()) {
        move(timestep, clock);
    } else {
        die(clock, view);
    }
}

//-----------------------------------------------------------
void Player::draw(sf::RenderWindow &window) {
    
    if (isAlive()) {
        window.draw(sprite);
        window.draw(afterBurnerSprite);
//        window.draw(collider);
    } else {
        if (!blast.empty()) {
            for (unsigned int i = 0; i < blast.size(); i++) {
                blast.at(i).draw(window);
            }
        }
    }
}







