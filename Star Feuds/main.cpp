// Incorporating copy constructors

#include "Game.hpp"

using namespace std;

int main(int, char const**) {

    Game game;
    game.run();
    
}
