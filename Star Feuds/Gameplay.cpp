//  Gameplay.cpp
//  Star Feuds
//
//  Created by William Molina on 3/28/16.
//  Copyright © 2016 William Molina. All rights reserved.

#include "Gameplay.hpp"

Gameplay::Gameplay(sf::Vector2u window, int playerLives, int playerLevel, int playerScore, int ngplus) : particle(window){
    
    winSize = window;
    lives = playerLives;
    level = playerLevel;
    score = playerScore;
    newGame = ngplus;
    enemyWaves = level * 2 * newGame;
    
    if (level == 5) {
        enemyWaves = 1;
    }
    
    bossStage = (level == 4);
    
    if (bossStage) {
        enemyWaves = 1;
    }
    
    taunts.push_back("Git gud, m8");
    taunts.push_back("LOL");
    taunts.push_back("U mad, bro?");
    taunts.push_back("Do you even lift?");
    taunts.push_back("I am your father!");
    taunts.push_back("Get rekt");
    taunts.push_back("360 no scope");
    taunts.push_back("Share, like, and subscribe");
    taunts.push_back("Play of the game");
    taunts.push_back("Houston, we have a problem");

}

//-----------------------------------------------------------------------------------
void Gameplay::init() {
    
    font.loadFromFile(resourcePath() + "lcd.ttf");
    font2.loadFromFile(resourcePath() + "RPGSystem.ttf");
    
    if (!starsTexture.loadFromFile(resourcePath() + "starynight.png")) {
        cout << "Error loading explosion texture!" << endl;
    }
    starsTexture.setRepeated(true);
    
    starsSprite.setTexture(starsTexture);
    starsSprite.setTextureRect(sf::IntRect(0,0,800,600));
    
    textScore.setFont(font);
    textScore.setString("");
    textScore.setCharacterSize(30);
    textScore.setColor(sf::Color::Red);
    textScore.setStyle(sf::Text::Bold);
    textScore.setPosition(10, 10);
    
    textTaunt.setFont(font2);
    textTaunt.setColor(sf::Color::Red);
    textTaunt.setCharacterSize(35);
    textTaunt.setString("");
    
    textScoreShadow = textScore;
    textScoreShadow.setPosition(textScoreShadow.getPosition().x + 1, textScoreShadow.getPosition().y + 1);
    textScoreShadow.setColor(sf::Color::White);
    
    textLives = textScore;
    textLives.setPosition((winSize.x / 2) - (100), textScore.getPosition().y);
    
    textLivesShadow = textScore;
    textLivesShadow.setPosition(textLives.getPosition().x + 1, textLives.getPosition().y + 1);
    textLivesShadow.setColor(sf::Color::White);
    
    if (bossStage) {
        textBossHealth = textLives;
        textBossHealth.setString("Boss Health: 000");
        textBossHealth.setPosition(winSize.x - textBossHealth.getLocalBounds().width - 10, textLives.getPosition().y);
        
        textBossHealthShadow = textLives;
        textBossHealthShadow.setColor(sf::Color::White);
        textBossHealthShadow.setPosition(textBossHealth.getPosition().x + 1, textBossHealth.getPosition().y + 1);
        
        particle.setParticleSpeed(250.f);
        particle.setGravity(0, .5f);
        
        hitBuffer.loadFromFile(resourcePath() + "hit.wav");
        hit.setBuffer(hitBuffer);
    }
    
    if (level == 5) {
        textBossHealth = textLives;
        textBossHealth.setString("Remaining: 000");
        textBossHealth.setPosition(winSize.x - textBossHealth.getLocalBounds().width - 10, textLives.getPosition().y);
        
        textBossHealthShadow = textLives;
        textBossHealthShadow.setColor(sf::Color::White);
        textBossHealthShadow.setPosition(textBossHealth.getPosition().x + 1, textBossHealth.getPosition().y + 1);
    }
    
    textMusicInfo = textLives;
    textMusicInfo.setFont(font2);
    textMusicInfo.setCharacterSize(25);
    textMusicInfo.setStyle(sf::Text::Regular);
    textMusicInfo.setColor(sf::Color::White);
    textMusicInfoShadow = textMusicInfo;
    textMusicInfoShadow.setColor(sf::Color::White);
    
    textShootTimer = textScore;
    
    textStatus = textScore;
    textStatusShadow = textStatus;
    textStatusShadow.setColor(sf::Color::White);
    
    string song = to_string(level);
    
//    if (level == 5) {
//        song = to_string(4);
//    }
    
    if (level < 4) {
        music.openFromFile(resourcePath() + song + ".ogg");
        music.setLoop(true);
    } else {
        music.openFromFile(resourcePath() + "first.ogg");
    }
    
    ships.emplace_back();
    ships.back().init(numExplosions, winSize);
    
    music.setVolume(75);
    music.play();
}

int Gameplay::getStatus() {
    return statusType;
}

//-----------------------------------------------------------------------------------
void Gameplay::loadEnemies() {
    
    if (!bossStage) {
        if (row.empty() && (enemyWaves > 0)) {
            
            enemyWaves--;
            for (unsigned int j = 0; j < ((level != 5) ? level : 3); j++) {
                
                int numEnemies = 10;
                
                if (level == 5) {
                    numEnemies = 100;
                }
                
                for (unsigned int i = 0; i < numEnemies; i++) {
                    float spacing = winSize.x / 10;
                    enemyList.emplace_back(sf::Vector2f(i * spacing, (j + 1) * spacing), level, (level * newGame));
                    if (level == 5) {
                        enemiesLeft++;
                    }
                }
                row.emplace_back(enemyList);
                enemyList.clear();
            }
        }
        
        if (row.empty() && (enemyWaves < 1)) {
            statusType = BEATLEVEL;
            
            if (!addedALife) {
                lives++;
                addedALife = true;
            }
        }
    } else {
        
        if (bossList.empty() && (enemyWaves > 0)) {
            
            enemyWaves--;
            float spacing = winSize.x / 10;
            bossList.emplace_back(sf::Vector2f(spacing, spacing), level, (level * newGame));
        }
        
        if (bossList.empty() && (enemyWaves < 1)) {
            statusType = BEATLEVEL;
            
            if (!addedALife) {
                lives++;
                addedALife = true;
            }
        }
    }
}
//-----------------------------------------------------------------------------------
void Gameplay::revive() {
    if (ships.empty()) {
        
        if (lives > 0) {
            lives--;
            if (lives < 1) {
                numExplosions = 25;
            } else {
                numExplosions = 1;
            }
            setTaunt = false;
            gameStart = false;
            initPrevFireTimer = false;
            displayTaunt = true;
            ships.emplace_back();
            ships.at(0).init(numExplosions, winSize);
        } else {
            statusType = GAMEOVER;
        }
    }
}

//-----------------------------------------------------------------------------------
void Gameplay::moveOnTimer(double &clock) {
    
    if (statusType != GAMEPLAY) {
        
        if (!initPrevEndTimer) {
            prevEndTimer = sf::seconds(clock);
            initPrevEndTimer = true;
        }
        
        endTimer -= sf::seconds(clock) - prevEndTimer;
        prevEndTimer = sf::seconds(clock);
        
        if (endTimer < sf::Time::Zero) {
            moveOn = true;
        }
    }
}

//-----------------------------------------------------------------------------------
bool Gameplay::getMoveOn() {
    return moveOn;
}

//-----------------------------------------------------------------------------------
int Gameplay::getLives() {
    return lives;
}

//-----------------------------------------------------------------------------------
int Gameplay::getScore() {
    return score;
}

//-----------------------------------------------------------------------------------
void Gameplay::setPaused(bool paused) {
    
    if (paused) {
        if (music.getStatus() == sf::Music::Playing) {
            music.pause();
        }
    } else {
        if (music.getStatus() == sf::Music::Paused) {
            music.play();
        }
    }
}

//-----------------------------------------------------------------------------------
void Gameplay::playerShoots() {
    if (!ships.empty() && ships.at(0).isAlive()) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            if (!shootButtonPressed) {
                
                playerPosition = ships.at(0).getPosition();
                clip.emplace_back(playerPosition, 1200);
                shootButtonPressed = true;
            }
        } else {
            shootButtonPressed = false;
        }
    }
}

//-----------------------------------------------------------------------------------
void Gameplay::enemiesShoot(double &clock) {
    
    // These two lines are for both the boss and regular enemies
    shotAccumulator += sf::seconds(clock) - prevShotAccumulator;
    prevShotAccumulator = sf::seconds(clock);

    if (!bossStage) {
        if (shotAccumulator > enemyShootTimer) {
            
            if (!row.empty()) {
                
                for (unsigned int j = 0; j < row.size(); j++) {
                    if (!row.at(j).empty()) {
                        for (int i = 0; i < row.at(j).size(); i++) {
                            if ((row.at(j).at(i).shoot()) && row.at(j).at(i).isAlive()) {
                                sf::Vector2f temp = row.at(j).at(i).getPosition();
                                enemyClip.emplace_back(temp, 800);
                                shotAccumulator = sf::Time::Zero;
                            }
                        }
                    }
                }
            }
        }
    } else {
        
        if (shotAccumulator > bossShootTimer) {
            if (!bossList.empty()) {
                if (bossList.back().shoot() && bossList.back().isAlive()) {
                    sf::Vector2f temp = bossList.back().getPosition();
                    enemyClip.emplace_back(temp, 1200);
                    shotAccumulator = sf::Time::Zero;
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------------
void Gameplay::updateBullets(kairos::Timestep &timestep, double &clock) {
    if (!clip.empty()) {
        for (int i = 0; i < clip.size(); i++) {
            double tempClock = clock;
            clip.at(i).update(level, 1, timestep, tempClock);
        
//          If the bullet goes off the screen, delete it
            if (clip.at(i).getPosition().y < 0) {
                clip.pop_front();
            }
        }
    }
}

//-----------------------------------------------------------------------------------
void Gameplay::updateEnemyBullets(kairos::Timestep &timestep, double &clock) {
    if (!enemyClip.empty()) {
        for (int i = 0; i < enemyClip.size(); i++) {
            enemyClip.at(i).update(level, -1, timestep, clock);
            
//          If the bullet goes off the screen, delete it
            if (enemyClip.at(i).getPosition().y > winSize.y) {
//                enemyClip.pop_front(); this works for the boss
                enemyClip.erase(enemyClip.begin() + i); // this works for everyone else
            }
        }
    }
}

//-----------------------------------------------------------------------------------
void Gameplay::checkCollisions(kairos::Timestep &timestep, sf::View &view, double &clock) {
    
    if (!bossStage) {
        if (!row.empty()) {
            
            for (unsigned int k = 0; k < row.size(); k++) {
                
                if (!row.at(k).empty()) {
                    for (unsigned int i = 0; i < row.at(k).size(); i++) {
                        row.at(k).at(i).update(timestep, view, clock);
                        if (displayTaunt) {
                            textTaunt.setPosition(row.at(0).at(0).getPosition().x + (row.at(0).at(0).getSize().x / 2)
                                                  - (textTaunt.getLocalBounds().width / 2),
                                                  row.at(0).at(0).getPosition().y + 40);
                        }
                        
                        if (!clip.empty()) {
                            for (int j = 0; j < clip.size(); j++) {
                                
                                if (row.at(k).at(i).isAlive()) {
                                    sf::FloatRect temp = clip.at(j).getBulletBox();
                                    if(row.at(k).at(i).collisionWithBullet(temp)) {
                                        if (level == 5) {
                                            enemiesLeft--;
                                        }
                                        clip.pop_front();
                                        score += 10 * newGame;
                                    }
                                }
                            }
                        }
                        
                        if (row.at(k).at(0).explosionIsOver()) {
                            row.at(k).pop_front();
                        }
                    }
                    if (row.at(k).empty()) {
                        row.erase(row.begin() + k);
                    }
                }
            }
        }
    } else {
        if (!bossList.empty()) {
            
            bossList.back().update(timestep, view, clock);
            bossList.back().direction(clock);
            if (displayTaunt) {
                textTaunt.setPosition(bossList.back().getPosition().x + (bossList.back().getSize().x / 2)
                                      - (textTaunt.getLocalBounds().width / 2),
                                      bossList.back().getPosition().y + 40);
            }
        
            if (!clip.empty()) {
                
                for (int j = 0; j < clip.size(); j++) {
                    
                    if (bossList.back().isAlive()) {
                        
                        sf::Vector2f tempPos = bossList.back().getPosition();
                        
                        sf::FloatRect temp = clip.at(j).getBulletBox();
                        if (bossList.back().collisionWithBullet(temp)) {
                            clip.pop_front();
                            score += 20 * newGame;
                            
                            tempPos.x += bossList.back().getSize().x / 2;
                            tempPos.y += bossList.back().getSize().y / 2;
                            
                            hit.play();
                            particle.setPosition(tempPos);
                            particle.fuel(800);
                            
                        }
                    }
                }
            }
            
            if (!bossList.back().isAlive() && !blowUp) {
            
                sf::Vector2f tempPos = bossList.back().getPosition();
                tempPos.x += bossList.back().getSize().x / 2;
                tempPos.y += bossList.back().getSize().y / 2;
                
                particle.setPosition(tempPos);
                particle.setGravity(0, 0);
                particle.fuel(2000);
                blowUp = true;
            }
            
            
            if (bossList.back().explosionIsOver()) {
                bossList.pop_front();
            }
        }
    }
}

//-----------------------------------------------------------------------------------
void Gameplay::checkPlayerCollisions(kairos::Timestep &timestep, sf::View &view, double &clock) {
    
    if (!ships.empty()) {
        if (!enemyClip.empty()) {
            for (unsigned int i = 0; i < enemyClip.size(); i++) {
                
                if (ships.at(0).isAlive()) {
                    sf::FloatRect temp = enemyClip.at(i).getBulletBox();
                    
                    if (ships.at(0).collisionWithBullet(temp)) {
                        enemyClip.pop_front();
                    }
                }
            }
        }
        
        if (ships.at(0).explosionIsOver()) {
            ships.pop_front();
        }
    }
}

//-----------------------------------------------------------------------------------
void Gameplay::moveRow(kairos::Timestep &timestep, sf::View &view, double &clock) {
    
    if (!bossStage) {
        if (!row.empty()) {
            for (unsigned int i = 0; i < row.size(); i++) {
                
                if (!row.at(i).empty()) {
                    for (unsigned int j = 0; j < row.at(i).size(); j++) {
                        
                        if (row.at(i).at(j).getPosition().x <= 0) {
                            row.at(i).at(j).setDirection(-1);
                        }
                        
                        if (row.at(i).at(j).getPosition().x + row.at(i).at(j).getSize().x >= winSize.x) {
                            row.at(i).at(j).setDirection(1);
                        }
                    }
                }
            }
        }
    } else {
        
        if (bossList.back().getPosition().x <= 0) {
            bossList.back().setDirection(-1);
        }
        
        if (bossList.back().getPosition().x + bossList.back().getSize().x >= winSize.x) {
            bossList.back().setDirection(1);
        }
    }
    
}

//-----------------------------------------------------------------------------------
void Gameplay::update(kairos::Timestep &timestep, sf::View &view, double &clock) {
    
    //----------------------------------------------------------- Mel's music stuff
    
    if (level > 3) {
        if (music.getStatus() == sf::Music::Stopped) {
            if (!initMainLoop) {
                music.openFromFile(resourcePath() + "main.ogg");
                music.setLoop(true);
                music.setVolume(75);
                music.play();
                initMainLoop = true;
            }
        }
    }
    
    
    //-----------------------------------------------------------
    
    
    if (!ships.empty()) {
        if ((lives == 0) && !ships.at(0).isAlive()) {
            if (music.getStatus() == sf::Music::Playing) {
                music.stop();
            }
        }
    }
    
    loadEnemies();
    
    if (!gameStart) {
        
        if (!initPrevFireTimer) {
            prevFireTimer = sf::seconds(clock);
            initPrevFireTimer = true;
        }
        
        fireAccumulator -= sf::seconds(clock) - prevFireTimer;
        prevFireTimer = sf::seconds(clock);
        
        if (fireAccumulator <= sf::Time::Zero) {
            fireAccumulator = fireTimer;
            gameStart = true;
        }
    } else {
        displayTaunt = false;
        
        if (!ships.empty()) {
            ships.at(0).update(timestep, view, clock);
        } else {
            displayTaunt = true;
        }
        
        playerShoots();
        enemiesShoot(clock);
        
        checkPlayerCollisions(timestep, view, clock);
    }
    
    moveRow(timestep, view, clock);

    updateBullets(timestep, clock);
    updateEnemyBullets(timestep, clock);

    checkCollisions(timestep, view, clock);
    
    revive();
    
    particle.update(timestep.getStepAsFloat());
    
    moveOnTimer(clock);

    manageTexts();
    showMusicInfo(clock);
    
    if (by < 256) {
        by+=2;
    } else {
        by = 0;
    }
    
    starsSprite.setTextureRect(sf::IntRect(0, -by, winSize.x, winSize.y));
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::F)) {
        if (!cheatButtonPressed) {
            lives++;
            cheatButtonPressed = true;
        }
    } else {
        cheatButtonPressed = false;
    }
    
}

//-----------------------------------------------------------------------------------
void Gameplay::manageTexts() {
    
    if (!gameStart && !setTaunt) {
        addTaunt();
        setTaunt = true;
    }
    
    stringScore = "Score:" + to_string(score);
    if (fireAccumulator > sf::seconds(1)) {
        stringShootTimer = "GET READY " + to_string(static_cast<int>(fireAccumulator.asSeconds()));
        textShootTimer.setString(stringShootTimer);
        textShootTimer.setPosition((winSize.x / 2) - (textShootTimer.getLocalBounds().width / 2), winSize.y / 2);
    } else {
        stringShootTimer = "DESTROY!";
        textShootTimer.setString(stringShootTimer);
        textShootTimer.setPosition((winSize.x / 2) - (textShootTimer.getLocalBounds().width / 2), winSize.y / 2);
    }
    
    stringLives = "Lives:" + to_string(lives);
    textScore.setString(stringScore);
    textScoreShadow.setString(textScore.getString());
    textLives.setString(stringLives);
    textLivesShadow.setString(stringLives);

    if (bossStage) {
        stringBossHealth = "Boss Health:";
        
        if (!bossList.empty()) {
            stringBossHealth += to_string(bossList.back().getHealth());
        } else {
            stringBossHealth += "0";
        }
        
        textBossHealth.setString(stringBossHealth);
        textBossHealthShadow.setString(stringBossHealth);
    }
    
    if (level == 5) {
        stringBossHealth = "Remaining:" + to_string(enemiesLeft);
        textBossHealth.setString(stringBossHealth);
        textBossHealthShadow.setString(stringBossHealth);
    }
    
    if (statusType == BEATLEVEL) {
        stringStatus = "YOU WIN!";
    }
    
    if (statusType == GAMEOVER) {
        stringStatus = "GAME OVER";
    }
    
    if (statusType != GAMEPLAY) {
        textStatus.setString(stringStatus);
        textStatusShadow.setString(stringStatus);
        textStatus.setPosition((winSize.x / 2) - (textStatus.getLocalBounds().width / 2), winSize.y / 2);
        textStatusShadow.setPosition(textStatus.getPosition().x + 1, textStatus.getPosition().y + 1);
    }
    
}

//-----------------------------------------------------------------------------------
void Gameplay::showMusicInfo(double &clock) {
    
    
    if (!startToFade) {
        
        if (!initPrevMusicInfotimer) {
            prevMusicInfoTimer = sf::seconds(clock);
            initPrevMusicInfotimer = true;
        }
        
        if (musicInfoTimerStart > sf::Time::Zero) {
            musicInfoTimerStart -= sf::seconds(clock) - prevMusicInfoTimer;
            prevMusicInfoTimer = sf::seconds(clock);
        } else {
            startToFade = true;
        }
    }
    
    if (displayMusicInfo) {
        
        stringMusicInfo = "Now Playing:\n";
        
        if (level == 1) {
            stringMusicInfo += "Dr. Crafty's Better Fortress\n";
            stringMusicInfo += "MATHGRANT\n";
            stringMusicInfo += "freemusicarchive.org/music/mathgrant";
        }
        
        if (level == 2) {
            stringMusicInfo += "La la triroriro\n";
            stringMusicInfo += "ROLEMUSIC\n";
            stringMusicInfo += "freemusicarchive.org/music/Rolemusic";
        }
        
        if (level == 3) {
            stringMusicInfo += "Sreda Vniecaps\n";
            stringMusicInfo += "ROLEMUSIC\n";
            stringMusicInfo += "freemusicarchive.org/music/Rolemusic";
        }
        
        if (level == 4 || level == 5) {
            stringMusicInfo += "Eclipse\n";
            stringMusicInfo += "MASTER MIND MEL SALAZAR\n";
            stringMusicInfo += "soundcloud.com/mel-salazar/eclipse";
        }
        
        textMusicInfo.setString(stringMusicInfo);
        textMusicInfoShadow.setString(stringMusicInfo);
        
        textMusicInfo.setPosition(20, (winSize.y - textMusicInfo.getLocalBounds().height) - 20);
        textMusicInfoShadow.setPosition(textMusicInfo.getPosition().x + .7, textMusicInfo.getPosition().y + .7);
        
        if (startToFade) {
            
            musicInfoTimer -= sf::seconds(clock) - prevMusicInfoTimer;
            prevMusicInfoTimer = sf::seconds(clock);
            
            sf::Color tempColor = textMusicInfo.getColor();
            tempColor.a = musicInfoTimer.asMilliseconds() / 10;
            textMusicInfo.setColor(tempColor);
            
            if (musicInfoTimer < sf::Time::Zero) {
                displayMusicInfo = false;
            }
        }
    }
}


//-----------------------------------------------------------------------------------
void Gameplay::draw(sf::RenderWindow &window) {
    
    window.draw(starsSprite);
    
    if (displayMusicInfo) {
        //    window.draw(textMusicInfoShadow);
        window.draw(textMusicInfo);
    }
    
    if (!ships.empty()) {
        ships.at(0).draw(window);
    }
    
    if (!bossStage) {
        for (unsigned int j = 0; j < row.size(); j++) {
            if (!row.at(j).empty()) {
                for (unsigned int i = 0; i < row.at(j).size(); i++) {
                    row.at(j).at(i).draw(window);
                }
            }
        }
    } else {
        bossList.back().draw(window);
        window.draw(textBossHealthShadow);
        window.draw(textBossHealth);
    }

    if (level == 5) {
        window.draw(textBossHealthShadow);
        window.draw(textBossHealth);
    }
    
    if (!clip.empty()) {
        for (unsigned int i = 0; i < clip.size(); i++) {
            clip.at(i).draw(window);
        }
    }
    
    if (!enemyClip.empty()) {
        for (unsigned int i = 0; i < enemyClip.size(); i++) {
            enemyClip.at(i).draw(window);
        }
    }

    window.draw(textScoreShadow);
    window.draw(textScore);
    window.draw(textLivesShadow);
    window.draw(textLives);
    
    if (!gameStart) {
        window.draw(textShootTimer);
    }
    
    if (displayTaunt) {
        window.draw(textTaunt);
    }
    
    if (statusType != GAMEPLAY) {
        window.draw(textStatusShadow);
        window.draw(textStatus);
    }
    
    window.draw(particle);
    
}

//-----------------------------------------------------------------------------------
void Gameplay::addTaunt() {
    
    int randNum = (rand() % (taunts.size() - 1));

    while (stringTaunt == taunts.at(randNum)) {
        randNum = (rand() % (taunts.size() - 1));
    }

    stringTaunt = taunts.at(randNum);
    
    textTaunt.setString(stringTaunt);
    
}

// Copy constructor
//-----------------------------------------------------------------------------------
Gameplay::Gameplay(const Gameplay &other) : particle(other.winSize) {

//    leaving it empty for now
//    was causing an error
    
}
