//  Credits.h
//  Erste
//
//  Created by William Molina on 6/1/15.
//  Copyright (c) 2015 William Molina. All rights reserved.

#ifndef __Erste__Credits__
#define __Erste__Credits__

//#include "ball.h"
#include "Explosion.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>

using namespace std;

class Credits
{
private:
    
    sf::Vector2f linearInterpolation(sf::Vector2f start, sf::Vector2f end, float alpha)
    {
        return (start * (1 - alpha) + end * alpha);
    }
    
    fstream ifs{resourcePath() + "credits", ios::in};
    sf::Font headingFont, textFont;
    sf::Text heading, text1, text2;
    sf::Vector2u winSize;
    
    float moveSpeed = 50.f;
    
    string line, content;
    bool isDone;
    
    bool exitCredits = false;
    
    deque< sf::Text > lineVector;
    vector< sf::Vector2f > prevPos;
    vector< sf::Vector2f > currentPos;
    
public:
    Credits(sf::Vector2u window);
    bool getIsDone();
    void populateVector();
    void update(kairos::Timestep &timeStep, double &clock);
    void draw(sf::RenderWindow &window);
};

#endif /* defined(__Erste__Credits__) */
