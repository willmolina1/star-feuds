//
//  Game.cpp
//  Star Feuds
//
//  Created by William Molina on 5/26/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#include "Game.hpp"

Game::Game() {
    
    window.create(sf::VideoMode(800, 600), "SFML window", sf::Style::Close);
    winSize = window.getSize();
    
    if (!icon.loadFromFile(resourcePath() + "icon.png")) {
        cout << "Error: could not load icon image" << endl;
    }
    
    if (!fontTitle.loadFromFile(resourcePath() + "starjedi.ttf")) {
        cout << "Could not load Title Font" << endl;
    }
    
    if (!font.loadFromFile(resourcePath() + "RPGSystem.ttf")) {
        cout << "Could not load RPGSystem Font" << endl;
    }
    
    window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
    
    view.setSize(sf::Vector2f(winSize.x, winSize.y));
    view.setCenter(winSize.x/2, winSize.y/2);
    
    timestep.setStep(1.f / 60.f);
    timestep.setMaxAccumulation(0.25);
    
    lives = 3;
    level = 1;
    score = 0;
    ngPlus = 1;
    
}

//-----------------------------------------------------------------------------------
void Game::processEvents() {
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
            window.close();
        }
        
        if (event.type == sf::Event::LostFocus) {
            paused = true;
        }
        
        if (event.type == sf::Event::GainedFocus) {
            paused = false;
        }
        
        }
}

//-----------------------------------------------------------------------------------
void Game::initialize() {
    
    switch (gameState) {
        case TITLE: {
            
            if (!credits.empty()) {
                credits.pop_front();
            }
            
            textTitle.setFont(fontTitle);
            textTitle.setCharacterSize(50);
            textTitle.setColor(sf::Color::Red);
            textTitle.setString("Star Feuds");
            textTitle.setPosition(floor((winSize.x / 2) - (textTitle.getLocalBounds().width / 2)),
                                  floor((winSize.y / 3) - (textTitle.getLocalBounds().height)));
            
            
            textPressStart.setFont(font);
            textPressStart.setCharacterSize(40);
            textPressStart.setColor(sf::Color::White);
            textPressStart.setString("Press Space");
            textPressStart.setPosition(floor((winSize.x / 2) - (textPressStart.getLocalBounds().width / 2)),
                                       floor(((winSize.y / 3) * 2 ) - (textPressStart.getLocalBounds().height)));
            
            text1 = textPressStart;
            text1.setCharacterSize(30);
            text1.setString("C - Credits");
            text1.setPosition(floor(((winSize.x / 5) * 1) - (text1.getLocalBounds().width / 2)),
                                    floor(((winSize.y / 5) * 4) - (text1.getLocalBounds().height)));
            
            text2 = text1;
            text2.setString("H - High Scores");
            text2.setPosition(floor(((winSize.x / 5) * 4) - (text2.getLocalBounds().width / 2)),
                                      text1.getPosition().y);
            
            text3 = text2;
            text3.setString("Esc - Quit Anytime \t v1.2");
            text3.setPosition(floor((winSize.x / 2) - (text3.getLocalBounds().width / 2)),
                              floor(((winSize.y / 5) * 5) - (text3.getLocalBounds().height * 2)));
            
            break;
        }
            
        case INTRO: {
            
            if (!gameplay.empty()) {
                gameplay.pop_back();
            }
            
            textTitle.setString("level " + to_string(level));
            textTitle.setPosition(floor((winSize.x / 2) - (textTitle.getLocalBounds().width / 2)),
                                  floor((winSize.y / 3) - (textTitle.getLocalBounds().height)));
            
            text1.setCharacterSize(30);
            text1.setString("Score: " + to_string(score));
            text1.setPosition(floor((winSize.x / 2) - (text1.getLocalBounds().width / 2)),
                              floor((textTitle.getPosition().y + (textTitle.getLocalBounds().height * 2))));
            
            text2.setString("Lives: " + to_string(lives));
            text2.setPosition(floor((winSize.x / 2) - (text2.getLocalBounds().width / 2)),
                              floor(text1.getPosition().y + (text2.getLocalBounds().height * 2)));
            
            if (ngPlus > 1) {
                text3 = text2;
                string temp("New Game+");

                if (ngPlus > 2) {
                    temp += to_string(ngPlus - 1);
                }
                
                text3.setString(temp);
                text3.setColor(sf::Color::Red);
                text3.setPosition(floor((winSize.x / 2) - (text3.getLocalBounds().width / 2)),
                                  floor(text2.getPosition().y + (text3.getLocalBounds().height * 2)));
            }
            
            break;
        }
            
        case DIALOGUE: {
            
//          re init timerIntro so that it's ready
//          to go on the next pass
//            timerIntro = sf::seconds(3);
            
            sf::Vector2u temp = window.getSize();
            dialogue.push_back(Dialogue(temp));
            dialogue.emplace_back(temp);
            dialogue.back().initiate();
            
            break;
        }
            
        case GAMEPLAY: {
            
            if (!dialogue.empty()) {
                dialogue.pop_back();
            }
            
            gameplay.emplace_back(winSize, lives, level, score, ngPlus);
            gameplay.back().init();
            break;
        }
            
        case GAMEOVER: {
            
            level = 1;
            ngPlus = 1;
            lives = 3;
            
            break;
        }
            
        case HIGHSCORE: {
            
            if (!gameplay.empty()) {
                gameplay.pop_back();
            }
            
            loadCurrentScores();
            
            updateHighScores();
            
            saveNewHighScores();
            
            displayHighScores();
            
            break;
        }
            
        case CREDITS: {
            credits.emplace_back(winSize);
            text1.setString("Space - skip");
            text1.setPosition(floor((winSize.x / 5) - (text1.getLocalBounds().width)),
                              floor(((winSize.y / 5) * 4 ) + text1.getLocalBounds().height));
            break;
        }
            
        default:
            break;
    }
    
}

//-----------------------------------------------------------------------------------
void Game::goToTitleScreen() {
    
    score = 0;
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        if (!selectionPressed) {
            
            gameState = INTRO;
            selectionPressed = true;
        }
    } else {
        selectionPressed = false;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::H)) {
        gameState = HIGHSCORE;
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
        gameState = CREDITS;
    }
    
}

//-----------------------------------------------------------------------------------
void Game::goToIntro() {
    
    if (timerIntro > sf::Time::Zero) {
        
        if (!initPrevTimerIntro) {
            prevTimerIntro = sf::seconds(timestep.getOverall());
            initPrevTimerIntro = true;
        }
        
        timerIntro -= sf::seconds(timestep.getOverall()) - prevTimerIntro;
        prevTimerIntro = sf::seconds(timestep.getOverall());
        
    } else {
        if (ngPlus < 2) {
            gameState = DIALOGUE;
        } else {
            gameState = GAMEPLAY;
        }
//      reset the timer
        timerIntro = sf::seconds(3);
        initPrevTimerIntro = false;
    }
}

//-----------------------------------------------------------------------------------
void Game::goToDialogue() {
   
    if (!dialogue.empty()) {
        if (!dialogue.back().getIsDone()) {
            string search = "!LEVEL" + to_string(level);
            double clock = timestep.getOverallAsFloat();
            
//            added v1.2
            if (!paused) {
                dialogue.back().update(search, timestep, clock);
            }
            
        } else {
            gameState = GAMEPLAY;
        }
    }
}

//-----------------------------------------------------------------------------------
void Game::goToGamePlay() {
    
    if (!paused) {
        if (!gameplay.empty()) {
            double clock = timestep.getOverallAsFloat();
            gameplay.back().update(timestep, view, clock);
            
            if (gameplay.back().getStatus() == 1) {
                if (gameplay.back().getMoveOn()) {
                    level++;
                    
                    if (level > 5) {
                        level = 1;
                        ngPlus++;
                    }
                    
                    score = gameplay.back().getScore();
                    lives = gameplay.back().getLives();
                    gameState = INTRO;
                }
            } else if (gameplay.back().getStatus() == 2) {
                if (gameplay.back().getMoveOn()) {
                    score = gameplay.back().getScore();
                    gameState = GAMEOVER;
                }
            }
        }
    } else {
        text1.setCharacterSize(50);
        text1.setString("Pause");
        text1.setPosition(floor((winSize.x / 2) - (text1.getLocalBounds().width / 2)),
                          floor((winSize.y / 2) - (text1.getLocalBounds().height)));
    }
    
    gameplay.back().setPaused(paused);
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
        if (!pausePressed) {
            paused = !paused;
            pausePressed = true;
        }
    } else {
        pausePressed = false;
    }
}

//-----------------------------------------------------------------------------------
void Game::goToGameOver() {
    
    gameState = HIGHSCORE;
    
}

//-----------------------------------------------------------------------------------
void Game::goToHighScore() {
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        if (!selectionPressed) {
            
            gameState = TITLE;
            selectionPressed = true;
        }
    } else {
        selectionPressed = false;
    }
}

//-----------------------------------------------------------------------------------
void Game::goToCredits() {
    
    if (!credits.empty()) {
        double clock = timestep.getOverallAsFloat();
        credits.back().update(timestep, clock);
        
        if (credits.back().getIsDone()) {
            gameState = TITLE;
        }
    }
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        if (!selectionPressed) {
            
            gameState = TITLE;
            selectionPressed = true;
        }
    } else {
        selectionPressed = false;
    }
    
}


//-----------------------------------------------------------------------------------
void Game::run() {
    while (window.isOpen()) {
     
        processEvents();
        
        fps.update();
        timestep.addFrame();
        
        if (prevGameState != gameState) {
            initialize();
            prevGameState = gameState;
        }
        
        while (timestep.isUpdateRequired()) { // this is true as long as there are unprocessed timesteps.
            
            switch (gameState) {
                case TITLE: {
                    goToTitleScreen();
                    break;
                }
                    
                case INTRO: {
                    goToIntro();
                    break;
                }
                    
                case DIALOGUE: {
                    goToDialogue();
                    break;
                }
                    
                case GAMEPLAY: {
                    goToGamePlay();
                    break;
                }
                    
                case GAMEOVER: {
                    goToGameOver();
                    break;
                }
                    
                case HIGHSCORE: {
                    goToHighScore();
                    break;
                }
                    
                case CREDITS: {
                    goToCredits();
                    break;
                }
                    
                default:
                    break;
            }
            
        } //---------------TIMESTEP
        
        // this doesn't exist anymore
//        clock.restart();
        
        std::string infoTitle{ windowTitle };
        infoTitle = "Fps: " + std::to_string(fps.getFps());
//        infoTitle += " | Time: " + std::to_string(timestep.getTime());
        infoTitle += " | Time: " + std::to_string(timestep.getTime());
        window.setTitle(infoTitle);
        
        draw();
    }
}

//-----------------------------------------------------------------------------------
void Game::draw() {
    
    // Clear screen
    window.clear();
    window.setView(view);
    
    drawState = gameState;
    
    if (prevGameState != gameState) {
        drawState = prevGameState;
    }
    
    switch (drawState) {
        case TITLE: {
            window.draw(textTitle);
            window.draw(textPressStart);
            window.draw(text1);
            window.draw(text2);
            window.draw(text3);
            break;
        }
            
        case INTRO: {
            window.draw(textTitle);
            window.draw(text1);
            window.draw(text2);
            if (ngPlus > 1) {
                window.draw(text3);
            }
            break;
        }
            
        case DIALOGUE: {
            
            if (!dialogue.empty()) {
                dialogue.back().draw(window);
            }
            
            break;
        }
            
        case GAMEPLAY: {
            
            if (!gameplay.empty()) {
                gameplay.back().draw(window);
            }
            
            if (paused) {
                window.draw(text1);
            }
            
            break;
        }
            
        case HIGHSCORE: {
            window.draw(textPressStart);
            window.draw(text1);
            window.draw(text2);
            window.draw(text3);
            break;
        }
            
        case CREDITS: {
            if (!credits.empty()) {
                credits.back().draw(window);
            }
            window.draw(text1);
            break;
        }
    }
    
    // Update the window
    window.display();

}

//-----------------------------------------------------------------------------------
void Game::loadCurrentScores() {
    
    fstream ifs;
    
    ifs.open(resourcePath() + "highscores.txt", ios::in);
    
    if (!highScores.empty()) {
        highScores.clear();
    }
    
    while (getline(ifs, line)) {
        highScores.push_back(stoi(line));
    }
    
    ifs.close();
    
}

//-----------------------------------------------------------------------------------
void Game::updateHighScores() {
    
    if (prevGameState == GAMEOVER) {
//        score = 0;
        
        text3 = text2;
        text3.setColor(sf::Color::Red);
        if (score > highScores.back()) {
            highScores.back() = score;
            text3.setString("NEW HIGH SCORE: " + to_string(score));
            swapHighScores();
        } else {
            text3.setString("Shoot for a higher score next time! Score: " + to_string(score));
        }
    } else {
        text3.setString("");
    }
}

//-----------------------------------------------------------------------------------
void Game::swapHighScores() {

    for (int i = static_cast<int>((highScores.size() - 1)); i > 0; i--) {
        if (highScores.at(i) > highScores.at(i - 1) ) {
            swap(highScores.at(i), highScores.at(i - 1));
        }
    }
    
}

//-----------------------------------------------------------------------------------
void Game::saveNewHighScores() {
    
    fstream ifs;
    
    ifs.open(resourcePath() + "highscores.txt", ios::out);
        
    for (int i = 0; i < highScores.size(); i++) {
        ifs << highScores.at(i) << "\n";
    }
    
    ifs.close();
}

//-----------------------------------------------------------------------------------
void Game::displayHighScores() {
    
    stringScores = "";
    
    textPressStart.setString("High Scores");
    textPressStart.setPosition(floor((winSize.x / 2) - (textPressStart.getLocalBounds().width / 2)),
                               floor((winSize.y / 3) - (textPressStart.getLocalBounds().height)));
    
    text2.setString("Space - Main Menu");
    text2.setPosition(floor((winSize.x / 2) - (text2.getLocalBounds().width / 2)),
                      floor(((winSize.y / 5) * 4) - (text2.getLocalBounds().height)));
    
    if (!highScores.empty()) {
        for (numScores = 0; numScores < highScores.size(); numScores++) {
            stringScores += to_string(numScores + 1) + ": " + to_string(highScores.at(numScores)) + "\n";
        }
    }
    
    text1.setCharacterSize(30);
    text1.setString(stringScores);
    text1.setPosition(floor((winSize.x / 2) - (text1.getLocalBounds().width / 2)),
                      floor(((winSize.y / 3) * 2 ) - (text1.getLocalBounds().height)));
    
    text3.setPosition(floor((winSize.x / 2) - (text3.getLocalBounds().width / 2)),
                      floor((winSize.y / 3) - (text1.getLocalBounds().height)));
    
}