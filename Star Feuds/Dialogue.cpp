//
//  Dialogue.cpp
//  Dialogue
//
//  Created by William Molina on 2/22/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#include "Dialogue.hpp"

using namespace std;

Dialogue::Dialogue(sf::Vector2u &window) {
    
    mWindowSize.x = window.x;
    mWindowSize.y = window.y;
    
    if (!font.loadFromFile(resourcePath() + "RPGSystem.ttf")) {
        cout << "Couldn't load font file!" << endl;
    }
    
    if (!ifs) {
        cout << "Could not load text file!" << endl;
    }
    
    if (!characterTexture.loadFromFile(resourcePath() + "players.png")) {
        cout << "Couldn't load texture file!" << endl;
    }
    
    if (!speech1Buffer.loadFromFile(resourcePath() + "speech1.wav")) {
        cout << "Couldn't load speech1!" << endl;
    }
    
    if (!speech2Buffer.loadFromFile(resourcePath() + "speech3.wav")) {
        cout << "Couldn't load speech2!" << endl;
    }
    
    boxPadding.x = 20.f;
    boxPadding.y = 10.f;
    
    dialogueBox.setSize(sf::Vector2f(500, 150));
    dialogueBox.setOutlineColor(sf::Color::Red);
    dialogueBox.setOutlineThickness(5.f);
    dialogueBox.setFillColor(sf::Color::Black);
    
    actorLabelBox = dialogueBox;
    actorLabelBox.setSize(sf::Vector2f(120.f, 40.f));
    
    text1.setFont(font);
    text1.setCharacterSize(35);
    text1.setColor(sf::Color::White);
    
    text2 = text1;
    text2.setString("");
    
    text3 = text2;
    text3.setString("");
    
    actorLabel = text3;
    actorLabel.setString("");
    
    arrow.setFont(font);
    arrow.setCharacterSize(40.f);
    arrow.setColor(sf::Color::Red);
    arrow.setStyle(sf::Text::Bold);
    arrow.setString("^");
    arrow.setOrigin(arrow.getLocalBounds().width / 2, arrow.getLocalBounds().height / 2);
    arrow.setRotation(180);

    // ********************** Textures and sprites
    player.setTexture(characterTexture);
    player.setTextureRect(sf::IntRect(0, 32, 64, 64));
    player.setScale(5.f, 5.f);
    
    enemy.setTexture(characterTexture);
    enemy.setTextureRect(sf::IntRect(320, 32, 64, 64));
    enemy.scale(5.f, 5.f);
    
    // ********************** Sound files
//    speech.setBuffer(speech1Buffer);
//    speech.setVolume(25);
}

//-----------------------------------------------------------
void Dialogue::initiate() {
    
    dialogueBox.setPosition((mWindowSize.x / 2) - (dialogueBox.getSize().x / 2), mWindowSize.y + dialogueBox.getOutlineThickness());
    dialogueBoxCurrentPosition = dialogueBox.getPosition();
    dialogueBoxFinalPosition = sf::Vector2f(150, 440);
    
    
    actorLabelBox.setPosition(-1000,
                              dialogueBox.getPosition().y
                              - actorLabelBox.getLocalBounds().height
                              + actorLabelBox.getOutlineThickness());
    
    player.setPosition(-player.getGlobalBounds().width + 10, 200);
    playerCurrentPosition = player.getPosition();
    
    enemy.setPosition(mWindowSize.x + enemy.getLocalBounds().width - 30, 200);
    enemyCurrentPosition = enemy.getPosition();
}

//-----------------------------------------------------------
void Dialogue::playIntro(kairos::Timestep &timestep) {
    
        if (dialogueBox.getPosition().y > dialogueBoxFinalPosition.y) {
            dialogueBoxPreviousPosition = dialogueBoxCurrentPosition;
            playerPreviousPosition = playerCurrentPosition;
            enemyPreviousPosition = enemyCurrentPosition;
            
            float dt{ timestep.getStepAsFloat() };
            
            dialogueBoxCurrentPosition.y -= actorMoveSpeed * dt;
            playerCurrentPosition.x += actorMoveSpeed * moveRate * dt;
            enemyCurrentPosition.x -= actorMoveSpeed * moveRate * dt;
            
            float interpolationAlpha{ timestep.getInterpolationAlphaAsFloat() };
            
            dialogueBox.setPosition(linearInterpolation(dialogueBoxPreviousPosition, dialogueBoxCurrentPosition, interpolationAlpha));
            player.setPosition(linearInterpolation(playerPreviousPosition, playerCurrentPosition, interpolationAlpha));
            enemy.setPosition(linearInterpolation(enemyPreviousPosition, enemyCurrentPosition, interpolationAlpha));
        } else {
            
            dialogueBox.setPosition(dialogueBoxCurrentPosition.x, ceil(dialogueBoxCurrentPosition.y));
            text1.setPosition(dialogueBox.getPosition().x + boxPadding.x, dialogueBox.getPosition().y + boxPadding.y);
            text2.setPosition(text1.getPosition().x, text1.getPosition().y + 35.f);
            text3.setPosition(text2.getPosition().x, text2.getPosition().y + 35.f);
            arrow.setPosition((dialogueBox.getPosition().x + dialogueBox.getLocalBounds().width) - (boxPadding.x * 2),
                              (dialogueBox.getPosition().y + dialogueBox.getLocalBounds().height) - (boxPadding.y * 2));
            actorLabelBox.setPosition(-1000,
                                      dialogueBox.getPosition().y
                                      - actorLabelBox.getLocalBounds().height
                                      + actorLabelBox.getOutlineThickness());
            step = FINDTEXT;
        }
}

//-----------------------------------------------------------
void Dialogue::exit(kairos::Timestep &timestep) {
    
    if (enemy.getPosition().x < mWindowSize.x) {
        // Get the actorLabelBox outta there!
        if (actorLabelBox.getPosition().x != -1000) {
            actorLabelBox.setPosition(-1000,
                                      dialogueBox.getPosition().y
                                      - actorLabelBox.getLocalBounds().height
                                      + actorLabelBox.getOutlineThickness());
        }
        
        dialogueBoxPreviousPosition = dialogueBoxCurrentPosition;
        playerPreviousPosition = playerCurrentPosition;
        enemyPreviousPosition = enemyCurrentPosition;
        
        float dt{ timestep.getStepAsFloat() };
        
        dialogueBoxCurrentPosition.y += actorMoveSpeed * dt;
        playerCurrentPosition.x -= actorMoveSpeed * moveRate * dt;
        enemyCurrentPosition.x += actorMoveSpeed * moveRate * dt;
        
        float interpolationAlpha{ timestep.getInterpolationAlphaAsFloat() };
        
        dialogueBox.setPosition(linearInterpolation(dialogueBoxPreviousPosition, dialogueBoxCurrentPosition, interpolationAlpha));
        player.setPosition(linearInterpolation(playerPreviousPosition, playerCurrentPosition, interpolationAlpha));
        enemy.setPosition(linearInterpolation(enemyPreviousPosition, enemyCurrentPosition, interpolationAlpha));
    } else {
        player.setPosition(-player.getGlobalBounds().width - boxPadding.x, player.getPosition().y);
        enemy.setPosition(mWindowSize.x + boxPadding.x, enemy.getPosition().y);
        dialogueBox.setPosition(dialogueBox.getPosition().x, mWindowSize.y + boxPadding.y);
        ifs.clear();
        ifs.seekg(0, ios::beg);
        ifs.close();
        ch = '\0';
        
        // Might need to remove this at the end, otherwise, it'll loop
        step = 0;
        
        if (enemy.getPosition().x > mWindowSize.x) {
            isDone = true;
        }
    }
}

//-----------------------------------------------------------
void Dialogue::animateArrow(double &clock) {
    
    if (arrowIsAnimated) {
        
        arrowAccumulator += sf::seconds(clock) - prevArrowAccumulator;
        prevArrowAccumulator = sf::seconds(clock);
        
        if (arrowAccumulator > arrowFrameTime) {
            displayArrow = true;
            if (arrowAccumulator > arrowMaxTime) {
                displayArrow = false;
                arrowAccumulator = sf::Time::Zero;
            }
        }
    } else {
        displayArrow = false;
    }
}

//-----------------------------------------------------------
void Dialogue::setActorLabelBox(string actor) {
    
    actorLabel.setString(actor);
    
    if (actor == "Caesar") {
        speech.setBuffer(speech2Buffer);
        speech.setVolume(20);
        actorLabelBox.setPosition(dialogueBox.getPosition().x, actorLabelBox.getPosition().y);
    } else {
        speech.setBuffer(speech1Buffer);
        speech.setVolume(10);
        actorLabelBox.setPosition(dialogueBox.getPosition().x
                                  + dialogueBox.getLocalBounds().width
                                  - actorLabelBox.getLocalBounds().width,
                                  actorLabelBox.getPosition().y);
    }
    
    float posX = ceil((actorLabelBox.getPosition().x + (actorLabelBox.getLocalBounds().width / 2) - (actorLabel.getLocalBounds().width / 2)) - actorLabelBox.getOutlineThickness());
    float posY = ceil(actorLabelBox.getPosition().y + (actorLabelBox.getLocalBounds().height / 2) - 30);
    
    actorLabel.setPosition(posX, posY);
}

//-----------------------------------------------------------
bool Dialogue::textWasFound() {
    return foundStartText;
}

//-----------------------------------------------------------
void Dialogue::findStartText(string search) {
    
    if (getline(ifs, line)) {
        curLine++;
        if (line.find(search, 0) != string::npos) {
            ifs.seekg(ifs.tellp());
            foundStartText = true;
            step = GETTEXT;
        }
    }
}

//-----------------------------------------------------------
void Dialogue::startDialogue() {
    getOneP = true;
}

//-----------------------------------------------------------
void Dialogue::streamText() {
    
    if (getOneP && (ch != '*')) {
        
        ifs >> noskipws >> ch;
        
        switch (ch) {
            case '\n': {
                lineCount++;
                break;
            }
                
            case '#': {
                getOneP = false;
                arrowIsAnimated = true;
                break;
            }
                
            case '^': {
                
                displayFirstLine = false;
                text2.setPosition(text2.getPosition().x, text2.getPosition().y - scrollAmount);
                text3.setPosition(text2.getPosition().x, text2.getPosition().y - scrollAmount);
                firstLine = secondLine;
                secondLine = thirdLine;
                thirdLine = "";
                text2.setPosition(text2.getPosition().x, text2.getPosition().y + scrollAmount);
                text3.setPosition(text2.getPosition().x, text2.getPosition().y + scrollAmount);
                displayFirstLine = true;
                lineCount = 1;
                break;
            }
                
            case '@': {
                // Discard the next two line breaks
                ifs.ignore();
                ifs.ignore();
                
                firstLine = secondLine = thirdLine =  "";
                lineCount = 0;
                arrowIsAnimated = false;
                break;
            }
                
            case '$': {
                while ((ifs >> ch) && (ch != '\n')) {
                        actorName += ch;
                }
                setActorLabelBox(actorName);
                actorName = "";
                break;
            }
                
            case '*': {
                firstLine = secondLine = thirdLine =  "";
                actorLabel.setString("");
                lineCount = 0;
                arrowIsAnimated = false;
                step = EXIT;
                break;
            }
                
            default: {
                if (ch != '*') {
                    if (lineCount == 0) {
                        firstLine += ch;
                    } else if (lineCount == 1) {
                        secondLine += ch;
                    } else if (lineCount == 2) {
                        thirdLine += ch;
                    }
//                    speech.play();
                    if (charCount % 4 == 0) {
                        speech.play();
                    }
                    charCount++;
                    if (charCount == 39) {
                        charCount = 0;
                    }
                    arrowIsAnimated = false;
                }
            }
        }
    }
}

//-----------------------------------------------------------
int Dialogue::getState() {
    return step;
}

//-----------------------------------------------------------
bool Dialogue::getIsDone() {
    return isDone;
}

//-----------------------------------------------------------
void Dialogue::getNewParagraph(double &clock) {
    
    if (getOneP) {
        accumulator += sf::seconds(clock) - prevAccumulator;
        prevAccumulator = sf::seconds(clock);
        
        if (accumulator > frameTime) {
            streamText();
            accumulator = sf::Time::Zero;
        }
    }
}

//-----------------------------------------------------------
void Dialogue::update(string search, kairos::Timestep &timestep, double &clock) {
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        
        if (!buttonPressed) {
            if (step == 0) {
                step = INTRO;
            } else {
                startDialogue();
            }
            buttonPressed = true;
        }
    } else {
        buttonPressed = false;
    }
    
    
    switch (step) {
        case INTRO: {
            playIntro(timestep);
            break;
        }
        case FINDTEXT: {
            findStartText(search);
            break;
        }
        case GETTEXT: {
            getNewParagraph(clock);
            animateArrow(clock);
            text1.setString(firstLine);
            text2.setString(secondLine);
            text3.setString(thirdLine);
            break;
        }
            
        case EXIT: {
            exit(timestep);
            break;
        }
    }
}

//-----------------------------------------------------------
void Dialogue::draw(sf::RenderWindow &window) {
    window.draw(player);
    window.draw(enemy);
    
    window.draw(dialogueBox);
    window.draw(actorLabelBox);
    if (displayFirstLine) {
        window.draw(text1);
    }
    window.draw(text2);
    window.draw(text3);
    window.draw(actorLabel);
    
    if (displayArrow) {
        window.draw(arrow);
    }
}