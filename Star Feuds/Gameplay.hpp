//
//  Gameplay.hpp
//  Star Feuds
//
//  Created by William Molina on 3/28/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#ifndef Gameplay_hpp
#define Gameplay_hpp

#include "Player.hpp"
#include "Boss.hpp"
#include "particle.hpp"

class Gameplay {
    
private:
    deque<Player> ships;
    deque<Bullet> clip;
    deque<Bullet> enemyClip;
    deque<Enemy> enemyList;
    deque<Boss> bossList;
    deque< deque<Enemy> > row;
    vector < string > taunts;
    ParticleSystem particle;
    
    int score;
    int lives;
    int level;
    int enemyWaves;
    int numExplosions = 1;
    int newGame;
    
    sf::Texture starsTexture;
    sf::Sprite starsSprite;
    int by = 0;
    
    sf::Font font;
    sf::Font font2;
    sf::Text textScore;
    sf::Text textScoreShadow;
    sf::Text textShootTimer;
    sf::Text textLives;
    sf::Text textLivesShadow;
    sf::Text textBossHealth;
    sf::Text textBossHealthShadow;
    sf::Text textMusicInfo;
    sf::Text textMusicInfoShadow;
    sf::Text textStatus;
    sf::Text textStatusShadow;
    sf::Text textTaunt;
    sf::Music music;
    sf::Vector2u winSize;
    float topPadding = 20;
    
    sf::SoundBuffer hitBuffer;
    sf::Sound hit;
    
    sf::Time shotAccumulator = sf::Time::Zero;
    sf::Time prevShotAccumulator; // new as of v1.2
    sf::Time enemyShootTimer = sf::seconds(1);
    sf::Time bossShootTimer = sf::seconds(.25);
    
    sf::Time prevFireTimer;
    bool initPrevFireTimer = false;
    sf::Time fireTimer = sf::seconds(4);
    sf::Time fireAccumulator = fireTimer;
    
    sf::Time musicInfoTimer = sf::seconds(2.5);
    sf::Time musicInfoTimerStart = sf::seconds(3);
    sf::Time prevMusicInfoTimer;
    bool initPrevMusicInfotimer = false;
    
    sf::Time endTimer = sf::seconds(4);
    sf::Time prevEndTimer;
    bool initPrevEndTimer = false;

    string stringScore, stringShootTimer, stringLives, stringBossHealth, stringMusicInfo, stringStatus, stringTaunt;
    
    bool cheatButtonPressed = false;
    
    bool buttonSPressed = false;
    bool shootButtonPressed = false;
    bool gameStart = false;
    bool displayMusicInfo = true;
    bool startToFade = false;
    bool moveOn = false;
    bool addedALife = false;
    bool bossStage = false;
    bool setTaunt = false;
    bool displayTaunt = false;
    bool blowUp = false;
    bool playedFirstPart = false;
    bool initMainLoop = false;
    enum status{GAMEPLAY, BEATLEVEL, GAMEOVER};
    int statusType = GAMEPLAY;
    
    sf::Vector2f playerPosition;
    int numEnemies = 0;
    int enemiesLeft = 0;
    
    void playerShoots();
    void enemiesShoot(double &clock);
    void updateBullets(kairos::Timestep &timestep, double &clock);
    void updateEnemyBullets(kairos::Timestep &timestep, double &clock);
    void checkCollisions(kairos::Timestep &timestep, sf::View &view, double &clock);
    void checkPlayerCollisions(kairos::Timestep &timestep, sf::View &view, double &clock);
    void moveRow(kairos::Timestep &timestep, sf::View &view, double &clock);
    void manageTexts();
    void showMusicInfo(double &clock);
    void loadEnemies();
    void revive();
    void moveOnTimer(double &clock);

    
public:
    Gameplay(sf::Vector2u window, int playerLives, int playerLevel, int playerScore, int ngplus);
    void init();
    int getStatus();
    bool getMoveOn();
    int getLives();
    int getScore();
    void setPaused(bool paused);
    void update(kairos::Timestep &timestep, sf::View &view, double &clock);
    void draw(sf::RenderWindow &window);
    void addTaunt();
//  copy constructors
    Gameplay(const Gameplay &right);
    
};

#endif /* Gameplay_hpp */
