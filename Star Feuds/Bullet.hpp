//
//  Bullet.hpp
//  Star Feuds
//
//  Created by William Molina on 2/15/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#ifndef Bullet_hpp
#define Bullet_hpp

#include "Explosion.hpp"

using namespace std;

class Bullet {
protected:
    sf::Vector2f linearInterpolation(sf::Vector2f start, sf::Vector2f end, float alpha)
    {
        return (start * (1 - alpha) + end * alpha);
    }
    
    sf::RectangleShape bullet;
    sf::Vector2f currentPosition;
    sf::Vector2f previousPosition;
    sf::FloatRect bulletBox;

    float speed;
    sf::SoundBuffer laserBuffer;
    sf::Sound laser;
    bool soundPlayed = false;
    
public:
    Bullet(sf::Vector2f &shooterPosition, float outsideSpeed);
    sf::Vector2f getPosition();
    sf::FloatRect getBulletBox();
//    void update(int level, int direction, kairos::Timestep &timestep, sf::Clock &clock);
    void update(int level, int direction, kairos::Timestep &timestep, double &clock);
    void draw(sf::RenderWindow &window);
};

#endif /* Bullet_hpp */
