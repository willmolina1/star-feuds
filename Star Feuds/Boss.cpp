//
//  Boss.cpp
//  Star Feuds
//
//  Created by William Molina on 6/2/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#include "Boss.hpp"

void Boss::direction(double &clock) {
    
    timerChangeDirection -= sf::seconds(clock) - prevChangeDirection;
    prevChangeDirection = sf::seconds(clock);
    
    if (timerChangeDirection < sf::Time::Zero) {
        
        int direction = (rand() % 2);
        
        if (direction == 0) {
            Enemy::setDirection(-1);
        } else if (direction == 1) {
            Enemy::setDirection(1);
        }
        
        timerChangeDirection = sf::seconds(1);
    }
    
    spriteSize = sf::Vector2f(48.f, 58.f);

    textureY = 214.f;
    
}

void Boss::setDirection(int dir) {
    
    Enemy::setDirection(dir);
}