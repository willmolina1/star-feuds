//
//  Enemy.hpp
//  Star Feuds
//
//  Created by William Molina on 3/3/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#ifndef Enemy_hpp
#define Enemy_hpp

#include "Bullet.hpp"

using namespace std;

class Enemy {

protected:
    sf::Vector2f linearInterpolation(sf::Vector2f start, sf::Vector2f end, float alpha)
    {
        return (start * (1 - alpha) + end * alpha);
    }
    
    sf::Texture texture;
    sf::Sprite sprite;
    
    sf::Vector2f currentPosition;
    sf::Vector2f previousPosition;
    sf::Vector2f spriteSize;
    
    sf::Time frameTimer;
    sf::Time prevTimer;
    sf::Time frameSwitch = sf::seconds(.1);
    
    float moveSpeed;
    int direction = 1;
    int health;
    int shootChance;
    float textureY;
    int textureX;
    
    deque <Explosion> blast;
    bool initBlast = false;
    bool explosionOver = false;
    bool initialize = false;
    
    int sound;
    
public:
    Enemy(sf::Vector2f position, int level, float speed);
    bool isAlive();
    int getHealth();
    void setSpeed(float speed);
    sf::Vector2f getPosition();
    sf::Vector2f getSize();
    bool collisionWithBullet(sf::FloatRect &bullet);
    void setDirection(int dir);
    int getDirection();
//    void animate(sf::Clock &clock);
    void animate(double &clock);
//    void move(kairos::Timestep &timestep, sf::Clock &clock);
    void move(kairos::Timestep &timestep, double &clock);
    
    bool shoot();
    void die(double &clock, sf::View &view);
    bool explosionIsOver();
//    void update(kairos::Timestep &timestep, sf::View &view, sf::Clock &clock);
    void update(kairos::Timestep &timestep, sf::View &view, double &clock);
    void draw(sf::RenderWindow &window);

    
};
#endif /* Enemy_hpp */
