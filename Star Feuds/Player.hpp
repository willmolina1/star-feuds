#ifndef Player_hpp
#define Player_hpp

#include "Bullet.hpp"

using namespace std;

class Player {
    
protected:
    
    sf::Vector2f linearInterpolation(sf::Vector2f start, sf::Vector2f end, float alpha)
    {
        return (start * (1 - alpha) + end * alpha);
    }
    
    sf::Texture texture;
    sf::Sprite sprite;
    
    sf::Texture afterBurnerTexture;
    sf::Sprite afterBurnerSprite;
    
    sf::Vector2f currentPosition;
    sf::Vector2f previousPosition;
    sf::Vector2f burnerCurrent;
    sf::Vector2f burnerPrev;
    sf::Vector2f spriteSize;
    sf::RectangleShape collider;
    sf::Vector2u windowSize;
    float bottomBuffer = 40.f;
    
    float moveSpeed = 400.f;
    float moveAbit;
    int health = 10;
    
    sf::Time frameTime;
    sf::Time prevTime;
    sf::Time frameSwitch = sf::seconds(.1);
    sf::Time explosionTimer = sf::seconds(.15);
    sf::Time accumulator = explosionTimer;
    
    int spriteDirection;
    int numExplosions;
    bool explosionOver = false;
    
    sf::Time afterPrevTimer;
    sf::Time afterAccumulator = sf::Time::Zero;
    sf::Time afterFrameTime = sf::seconds(.07f);
    int afterFrame = 0;
    
//    sf::Time shootTimer;
    
    bool shootButtonIsPressed = false;
    
    sf::SoundBuffer hitBuffer;
    sf::Sound hit;
    
    deque<Explosion> blast;
    bool initBlast = false;
    
public:
    Player();
    void init(int explosions, sf::Vector2u window);
    bool isAlive();
    const sf::Vector2f getPosition();
    sf::Vector2f getSize();
    sf::Sprite getSprite();
//    int rotate(sf::Clock &clock);
    int rotate(double clock);
    int getHealth();
    void takeDamage(int damage);
    bool collisionWithBullet(sf::FloatRect &bullet);
    bool explosionIsOver();
//    void die(sf::Clock &clock, sf::View &view);
    void die(double clock, sf::View &view);
    void move(kairos::Timestep &timestep, double clock);
//    void animateAfterBurner(sf::Clock &clock);
    void animateAfterBurner(double clock);
//    void update(kairos::Timestep &timestep, sf::View &view, sf::Clock &clock);
    void update(kairos::Timestep &timestep, sf::View &view, double clock);
    void draw(sf::RenderWindow &window);
    
    
    
};

#endif /* Player_hpp */
