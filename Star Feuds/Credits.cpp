//  Credits.cpp
//  Erste
//
//  Created by William Molina on 6/1/15.
//  Copyright (c) 2015 William Molina. All rights reserved.

#include "Credits.h"

Credits::Credits(sf::Vector2u window) {
    
    winSize = window;
    
    // Heading font
    if (!headingFont.loadFromFile(resourcePath() + "starjedi.ttf")) std::cout << "Error loading font!" << std::endl;
    
    // Regular text font
    if (!textFont.loadFromFile(resourcePath() + "RPGSystem.ttf")) std::cout << "Error loading font!" << std::endl;
    
    // Load the file
    if (!ifs) std::cout << "Could not load credits file!" << endl;
    
    // Initialize the texts
    heading.setFont(headingFont);
    heading.setCharacterSize(40);
    heading.setColor(sf::Color::Red);
    
    text1.setFont(textFont);
    text1.setCharacterSize(35);
    text1.setColor(sf::Color::White);
    
    text2.setFont(textFont);
    text2.setCharacterSize(20);
    text2.setColor(sf::Color::Red);
    
    populateVector();

}

bool Credits::getIsDone() {
    return isDone;
}

void Credits::populateVector() {
    
    // Reset the file's pointer
    ifs.clear();
    ifs.seekg(0L, ios::beg);
    
    // This sets the vertical alignment for the texts
    float textVerticalPosition = winSize.y;
    
    while (getline(ifs, line)) {
        // Figure out if it's a heading or a text
        int pound = static_cast<int>(line.rfind("#"));
        int star = static_cast<int>(line.rfind("*"));
        int per = static_cast<int>(line.rfind("%"));
        
        // Push back headings into the vector
        if (pound == 0) {
            
            content = line.substr(pound + 1, line.size());
            heading.setString(content);
            heading.setPosition(round((winSize.x / 2) - ((heading.getLocalBounds().width) / 2)), textVerticalPosition);
            lineVector.push_back(heading);
            
            textVerticalPosition += 50;
        }
        
        // Push back texts into the vector
        if (star == 0) {
            
            content = line.substr(star + 1, line.size());
            text1.setString(content);
            text1.setPosition(round((winSize.x / 2) - ((text1.getLocalBounds().width) / 2)), textVerticalPosition);
            lineVector.push_back(text1);
            
            textVerticalPosition += 30;
        }
        
        // Push back second type of text into the vector
        if (per == 0) {
            
            content = line.substr(per + 1, line.size());
            text2.setString(content);
            text2.setPosition(round((winSize.x / 2) - ((text2.getLocalBounds().width) / 2)), textVerticalPosition);
            lineVector.push_back(text2);
            
            textVerticalPosition += 20;
        }
        
        // Add spacing if there is a line space
        if (line.empty()) {
            textVerticalPosition += 30;
        }
    }
    
    ifs.close();
    
    prevPos.resize(lineVector.size());
    currentPos.resize(lineVector.size());
}

void Credits::update(kairos::Timestep &timestep, double &clock) {
    
    if (!lineVector.empty()) {
        
        for (unsigned int i = 0; i < lineVector.size(); i++) {

            lineVector.at(i).move(0, -1);
            
            if ((lineVector.at(i).getPosition().y + lineVector.at(i).getLocalBounds().height + 20) < 0) {
                lineVector.pop_front();
            }
        }
    }
    
    if (lineVector.empty()) {
        lineVector.clear();
        isDone = true;
    }
    
}

void Credits::draw(sf::RenderWindow &window) {
    
    if (!lineVector.empty()) {
        for (unsigned int i = 0; i < lineVector.size(); i++) {
            window.draw(lineVector.at(i));
        }
    }
    
}






