//
//  Enemy.cpp
//  Star Feuds
//
//  Created by William Molina on 3/3/16.
//  Copyright © 2016 William Molina. All rights reserved.
//

#include "Enemy.hpp"

Enemy::Enemy(sf::Vector2f position, int level, float speed) {
    
    srand(static_cast<unsigned int>(time(NULL)));

    health = 10;
    
//  "speed" is actually the level, which is why I'm doing the following:
//  this "textureY" variable will be used for the enemy sprites

    if (level == 1) {
        textureY = 144.f;
    } else if (level == 2) {
        textureY = 320.f;
    } else if (level == 3) {
        textureY = 496.f;
    } else if (level == 5) {
        textureY = 672.f;
    }
    
    textureX = 0;
    
    if (level == 5) {
        level = 20;
    }
    
    moveSpeed = 30 * speed;
    
    spriteSize = sf::Vector2f(48.f, 30.f);

    
    sprite.setPosition(position);
    
    currentPosition = sprite.getPosition();
    
    if (!texture.loadFromFile(resourcePath() + "enemies.png")) {
        cout << "Error loading Enemy texture!" << endl;
    }
    
//  if it's level 5, lower the shooting "chance" to just 10%
    if (level == 5) {
        shootChance = 10;
    } else {
        shootChance = 20;
    }

    sound = 0;
    
}

//-----------------------------------------------------------
bool Enemy::isAlive() {
    if (health > 0) {
        return true;
    } else {
        return false;
    }
}

//-----------------------------------------------------------
int Enemy::getHealth() {
    return health;
}

//-----------------------------------------------------------
void Enemy::setSpeed(float speed) {
    moveSpeed = speed;
}


//-----------------------------------------------------------
sf::Vector2f Enemy::getPosition() {
    return sprite.getPosition();
}

//-----------------------------------------------------------
sf::Vector2f Enemy::getSize() {
    return sf::Vector2f(sprite.getLocalBounds().width, sprite.getLocalBounds().height);
}

//-----------------------------------------------------------
bool Enemy::collisionWithBullet(sf::FloatRect &bullet) {
    if (bullet.top < (sprite.getPosition().y + sprite.getLocalBounds().height)) {
        
        if (sprite.getPosition().x < bullet.left + bullet.width &&
            sprite.getPosition().x + sprite.getLocalBounds().width > bullet.left) {
            health -= 10;
            
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//-----------------------------------------------------------
void Enemy::setDirection(int dir) {
    direction = dir;
    
    if (moveSpeed > 290) {
        moveSpeed = 150;
    }
    
}

//-----------------------------------------------------------
int Enemy::getDirection() {
    return direction;
}

//-----------------------------------------------------------
void Enemy::animate(double &clock) {
    
    frameTimer += sf::seconds(clock) - prevTimer;
    prevTimer = sf::seconds(clock);
    
    if (frameTimer > frameSwitch) {
        textureX++;
        if (textureX == 4) {
            textureX = 0;
        }
        frameTimer = sf::Time::Zero;
    }
    
}

//-----------------------------------------------------------
void Enemy::move(kairos::Timestep &timestep, double &clock) {
    
    if (!initialize) {
        // Ship stuff
        sprite.setTexture(texture);
        sprite.setTextureRect(sf::IntRect(textureX, textureY, spriteSize.x, spriteSize.y));
        sprite.setScale(sf::Vector2f(1.2f, 1.2f));
        initialize = true;
    }
    
    previousPosition = currentPosition;
    float dt{ timestep.getStepAsFloat() };
    
    currentPosition.x -= moveSpeed * direction * dt;
    
    float interpolationAlpha{ timestep.getInterpolationAlphaAsFloat() };
    
    animate(clock);
    
    sprite.setTextureRect(sf::IntRect(textureX * 48.f, textureY, spriteSize.x, spriteSize.y));
    
    sprite.setPosition(linearInterpolation(previousPosition, currentPosition, interpolationAlpha));
    
}

//-----------------------------------------------------------
bool Enemy::shoot() {
    
    return (rand() % 100) < shootChance;
}

//-----------------------------------------------------------
void Enemy::die(double &clock, sf::View &view) {

    if (!initBlast) {
        int rotation = rand() % 360;
        blast.push_back(Explosion(sound, currentPosition, spriteSize, rotation));
        initBlast = true;
    }
    
    if (!blast.empty()) {
        blast.at(0).update(clock, view);
        
        if (blast.at(0).isOver()) {
            blast.pop_front();
            explosionOver = true;
        }
    }
}

//-----------------------------------------------------------
bool Enemy::explosionIsOver() {
    return explosionOver;
}

//-----------------------------------------------------------
void Enemy::update(kairos::Timestep &timestep, sf::View &view, double &clock) {
    
    if (isAlive()) {
        move(timestep, clock);
    } else {
        die(clock, view);
    }
}

//-----------------------------------------------------------
void Enemy::draw(sf::RenderWindow &window) {
    
    if (isAlive()) {
        window.draw(sprite);
    } else {
        if (!blast.empty()) {
            blast.at(0).draw(window);
        }
    }
}